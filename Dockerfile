FROM python:3.10.7-slim-buster
RUN apt-get update && apt-get -y install curl

RUN pip install requests

RUN curl -sL https://github.com/openfaas/faas/releases/download/0.9.14/fwatchdog > /usr/bin/fwatchdog \
    && chmod +x /usr/bin/fwatchdog

COPY entrypoint2.py .
ENV fprocess="python3 entrypoint2.py"

EXPOSE 8080
CMD [ "fwatchdog" ]