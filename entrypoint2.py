import requests
import sys

inputs = sys.stdin.readline().split(',')
print('Creating md5 hash of:', inputs[0], '  using version: <',inputs[2],'> of the microservices.')

SERVICE1_URL = 'http://'+inputs[1]+':8080'

site = inputs[0]
message = requests.get(site).text

data = ["md5", message]

r = requests.post(SERVICE1_URL, data="\n".join(data))
print(r.text) 

